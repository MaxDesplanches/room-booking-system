import launchDatabase from './connectivity/database';
import launchServer from './server';

async function start() {
  await launchDatabase();
  await launchServer();
}

start();
