import mongoose from 'mongoose';
import config from '../config';
import logger from '../logger/logger';

const launchDatabase = async (): Promise<void> => new Promise<void>((resolve, reject) => {
  const mongodbUrl = config.mongoDB.url;
  mongoose.connect(`${String(mongodbUrl)}/booking_service`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    user: config.mongoDB.user,
    pass: config.mongoDB.pass,
  });
  const db = mongoose.connection;
  db.on('error', (error: Error) => {
    logger.error(error.message);
    reject(error);
  });
  db.once('open', () => {
    logger.info(`Connected to database ${mongodbUrl}`);
    resolve();
  });
});

export default launchDatabase;
