/* eslint-disable consistent-return */
/* eslint-disable no-underscore-dangle */
import { CallbackError } from 'mongoose';
import logger from '../logger/logger';
import { IBooking, IRoom, Room } from '../models/room';

/**
* remove a booking for a specific room
*
* @param {string} roomId room id to take the meeting
* @param {Date} startDate start date for the meeting
* @param {Date} endDate end date for the meeting
* @param {string} email email for the organizer
*/
export const removeRoom = async (roomId: string, startDate: Date, endDate: Date, email: string): Promise<any[] | void> => new Promise<any[]>((resolve, reject) => {
  const filter = { _id: roomId, booking: { $elemMatch: { emailOrganize: email, startDate, endDate } } };
  Room.findOne(filter, (err: CallbackError, doc: any) => {
    if (err || !doc) {
      reject(err);
    }
    resolve(doc);
  });
});

/**
* database request to get all rooms
*/
export const findRooms = async (): Promise<any[]> => new Promise<any[]>((resolve, reject) => {
  // TODO: send only rooms from same company
  Room.find({}, { booking: { _id: 0 } }, null, (err: CallbackError, rooms: any[]) => {
    if (err) {
      reject(err);
    }
    resolve(rooms);
  });
});

/**
* database request to book a room after check availability
*
* @param {string} roomId room id to take the meeting
* @param {IRoom} room room to update
* @param {Date} startDate start date for the meeting, end date will be calculated (1 hour)
* @param {string} email email for the organizer
*/
export const bookRoom = async (roomId: string, room: IRoom, startDate: Date, email: string) => {
  const book: IBooking = {
    emailOrganizer: email,
    startDate,
    endDate: new Date(new Date(startDate).setHours(startDate.getHours() + 1)),
  };
  room.booking.push(book);
  await Room.findByIdAndUpdate(roomId, room);
};

/**
* simple check for one room if booking is available to the given date and time
*
* @param {IRoom} room room to check
* @param {Date} date start date for the meeting, end date will be calculated (1 hour)
*/
const isRoomAvailable = (room: IRoom, date: Date): boolean => {
  const booking = room.booking.map((book: IBooking) => {
    const { startDate, endDate } = book;
    const startDateWished: Date = date;
    const endDateWished: Date = new Date(new Date(date).setHours(date.getHours() + 1));
    // check if date is completely before the shedule from the room
    const dateWishedIsBefore: boolean = startDateWished.getTime() > startDate.getTime()
      && endDateWished.getTime() > endDate.getTime();
    // check if date is completely after the shedule from the room
    const dateWishedIsAfter: boolean = startDateWished.getTime() < startDate.getTime()
      && endDateWished.getTime() < endDate.getTime();
    return dateWishedIsBefore || dateWishedIsAfter;
  });
  // return false if the date whished is already taken in the room booking array
  return booking.every((book) => book);
};

/**
* simple check if a room from all rooms is available to the given date and time
*
* @param {IRoom} room room to check
* @param {Date} date start date for the meeting, end date will be calculated (1 hour)
*/
export const simpleCheckRoomsAvailability = async (date: Date): Promise<IRoom | void> => new Promise((resolve, reject) => {
  Room.find({}, null, null, (err: CallbackError, rooms: any[]) => {
    if (err) {
      return reject(err);
    }
    rooms.forEach((room: IRoom) => {
      // TODO: improve algorythms (see README)
      const isAvailable: boolean = isRoomAvailable(room, date);
      if (isAvailable) {
        logger.info(`room is available: ${room._id}`);
        return resolve(room);
      }
    });
    return resolve();
  });
});
