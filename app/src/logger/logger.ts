import winston from 'winston';

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  defaultMeta: { service: 'user-service' },
});

if (process.env.NODE_ENV === 'production') {
  logger.add(new winston.transports.File({ filename: 'production.log' }));
} else {
  logger.add(new winston.transports.Console());
}

export default logger;
