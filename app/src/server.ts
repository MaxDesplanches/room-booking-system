/* eslint-disable import/no-mutable-exports */
import express from 'express';
import * as path from 'path';
import rateLimit from 'express-rate-limit';
import { Server } from 'http';
import config from './config';
import logger from './logger/logger';
import accountRouter from './routers/account';
import roomRouter from './routers/room';

const limiter = rateLimit(config.rateLimit);

const YAML = require('yamljs');
const swaggerUi = require('swagger-ui-express');

let swaggerDocument = {};

if (config.server.environment === 'dev') {
  swaggerDocument = YAML.load(path.resolve(__dirname, './../../documentation.yaml'));
}
export const app = express();
export let server: Server = new Server();

const launchServer = async () => {
  const serverPort = config.server.port;
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));
  app.use(limiter);
  app.use('/room', roomRouter);
  app.use('/account', accountRouter);
  if (config.server.environment === 'dev') {
    app.use('/api-documentation', swaggerUi.serve);
    app.get('/api-documentation', swaggerUi.setup(swaggerDocument));
  }

  server = app.listen(serverPort, () => {
    logger.info(`Server started on port ${serverPort}`);
    return Promise.resolve();
  });
};

export default launchServer;
