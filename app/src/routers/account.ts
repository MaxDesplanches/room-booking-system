import express, { Router } from 'express';
import jwt from 'jsonwebtoken';
import { CallbackError } from 'mongoose';
import config from '../config';
import sendError from '../lib/error';
import { Account, queryValidator } from '../models/account';

const accountRouter: Router = express.Router();

accountRouter.post('/login', queryValidator, async (req: express.Request, res: express.Response) => {
  const { login, password } = req.body;
  await Account.findOne({ login, password }, null, null, (err: CallbackError, doc: any) => {
    if (err || !doc) {
      return sendError(res, 404, 'account not found');
    }
    const accessToken = jwt.sign({ email: login }, String(config.accessTokenSecret), { expiresIn: '24h' });
    return res.json({ accessToken });
  });
});

export default accountRouter;
