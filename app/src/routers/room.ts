import express, { Router } from 'express';
import { CallbackError } from 'mongoose';
import authenticateJWT from '../lib/auth';
// import getCompanyFirstLetterFromEmail from '../lib/company';
import sendError from '../lib/error';
import logger from '../logger/logger';
import { IRoom } from '../models/room';
import {
  bookRoom, findRooms, removeRoom, simpleCheckRoomsAvailability,
} from '../service/room';

const roomRouter: Router = express.Router();

roomRouter.post('/', authenticateJWT, async (req: express.Request, res: express.Response) => {
  const { email } = res.locals;
  const { startingDate } = req.body;
  const date: Date = new Date(startingDate);
  // TODO: to implemented to choose good side of room
  // const companyFirstLetter:string = getCompanyFirstLetterFromEmail(email);

  const room: IRoom | void = await simpleCheckRoomsAvailability(date);
  if (room) {
    // eslint-disable-next-line no-underscore-dangle
    bookRoom(room._id, room, date, email);
    return res.json(room);
  }
  return sendError(res, 404, 'No room available');
});

roomRouter.get('/', authenticateJWT, async (req: express.Request, res: express.Response) => {
  findRooms()
    .then((rooms: any[]) => res.json(rooms))
    .catch((err: CallbackError) => {
      logger.error(err);
      return sendError(res, 404, 'rooms not found');
    });
});

roomRouter.delete('/', authenticateJWT, async (req: express.Request, res: express.Response) => {
  const { email } = res.locals;
  const { id, startDate } = req.query;
  const date: Date = new Date(String(startDate));
  const endDate: Date = new Date(new Date(date).setHours(date.getHours() + 1));

  removeRoom(String(id), date, endDate, email)
    .then(() => res.status(204).send())
    .catch((err: CallbackError) => {
      logger.error(err);
      return sendError(res, 404, `room ${id} not found with start Date ${startDate}`);
    });
});

export default roomRouter;
