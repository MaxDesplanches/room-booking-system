const config = {
  mongoDB: {
    url: process.env.MONGODB_URL,
    user: process.env.MONGODB_USER,
    pass: process.env.MONGODB_PASS,
  },
  server: {
    port: process.env.SERVER_PORT,
    environment: process.env.NODE_ENV,
  },
  accessTokenSecret: process.env.ACCESS_TOKEN_SECRET,
  rateLimit: {
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 100, // limit each IP to 100 requests per windowMs
  },
};

export default config;
