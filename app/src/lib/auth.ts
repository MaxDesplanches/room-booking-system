import express from 'express';
import jwt, { JwtPayload } from 'jsonwebtoken';
import config from '../config';

const authenticateJWT = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
  const authHeader = req.headers.authorization;
  if (authHeader) {
    const token = authHeader.split(' ')[1];

    try {
      const decoded: string | JwtPayload = jwt.verify(token, String(config.accessTokenSecret));
      if (decoded) {
        // eslint-disable-next-line dot-notation
        res.locals.email = (decoded as JwtPayload)['email'];
        return next();
      }
    } catch (error) {
      return res.sendStatus(401);
    }
  }
  return res.sendStatus(403);
};

export default authenticateJWT;
