const getCompanyFirstLetterFromEmail = (email: string): string => {
  const domain = email.replace(/.*@/, '');
  return domain[0];
};

export default getCompanyFirstLetterFromEmail;
