import express from 'express';
import { validationResult } from 'express-validator';

const sendError = (res: express.Response, statusCode: number, msg: string) => {
  const error = {
    error: {
      msg,
      statusCode,
    },
  };
  res.status(statusCode).json(error);
};

// eslint-disable-next-line max-len
export const validation = (req: express.Request, res: express.Response, next: express.NextFunction) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  return next();
};

export default sendError;
