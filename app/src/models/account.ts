import { query } from 'express-validator';
import mongoose, { Schema } from 'mongoose';

export const accountSchema = new Schema({
  login: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
}, {
  versionKey: false,
});

export const Account = mongoose.model('accounts', accountSchema);

export const queryValidator = [query('login').isString(), query('password').isString()];
