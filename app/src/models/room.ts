import mongoose, { Schema } from 'mongoose';

export interface IBooking {
  emailOrganizer: string;
  startDate: Date;
  endDate: Date;
}

export interface IRoom {
  _id: string;
  name: string;
  booking: IBooking[];
}

const bookingSchema = new Schema<IBooking>({
  emailOrganizer: {
    type: String,
    required: true,
  },
  startDate: {
    type: Date,
    required: true,
  },
  endDate: {
    type: Date,
    required: true,
  },
});

export const roomSchema = new Schema<IRoom>({
  name: {
    type: String,
    required: true,
  },
  booking: [bookingSchema],
}, {
  versionKey: false,
});

export const Room = mongoose.model('room', roomSchema);
export const Booking = mongoose.model('booking', bookingSchema);

// TODO: to use as middleware in router
// export const queryValidator = [body('startDate').isDate()];
