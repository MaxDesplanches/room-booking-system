/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-undef */
import mongoose from 'mongoose';
import { Account } from '../src/models/account';
import launchServer, { app, server } from '../src/server';
import accountCoca from './fixtures/account';

const request = require('supertest');

// TODO: to use common function between test file
const connectTestDb = async () => {
  const uri = process.env.MONGO_URL;
  mongoose.connect(`${String(uri)}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  const db = mongoose.connection;
  db.once('open', () => Promise.resolve());
  db.on('error', (error: Error) => Promise.reject(error));
};

describe('Account Router', () => {
  beforeAll(async () => {
    await connectTestDb();
    const account = new Account(accountCoca);
    await account.save();
    await launchServer();
  });

  afterAll(async () => {
    mongoose.connection.close();
    server.close();
  });

  it('should login with access token in response', async () => {
    const response = await request(app)
      .post('/account/login')
      .set('Content-type', 'application/json')
      .send(accountCoca);

    expect(JSON.stringify(response.body)).toContain('accessToken');
  });
});
