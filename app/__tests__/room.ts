/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-undef */
import mongoose from 'mongoose';
import { Room } from '../src/models/room';
import launchServer, { app, server } from '../src/server';
import accountCoca from './fixtures/account';
import { cocaRoom, pepsiRoom } from './fixtures/room';

jest.setTimeout(20000);

const request = require('supertest');

// TODO: to use common function between test file
const connectTestDb = async () => {
  const uri = process.env.MONGO_URL;
  mongoose.connect(`${String(uri)}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  const db = mongoose.connection;
  db.once('open', () => Promise.resolve());
  db.on('error', (error: Error) => Promise.reject(error));
};

describe('Room Router', () => {
  beforeAll(async () => {
    await connectTestDb();
    await Room.deleteMany();
    cocaRoom.forEach((room) => new Room(room).save());
    pepsiRoom.forEach((room) => new Room(room).save());
    await launchServer();
  });

  afterAll(async () => {
    mongoose.connection.close();
    server.close();
  });

  it('should be able to login as coca user and to get all room', async () => {
    const accountResponse = await request(app)
      .post('/account/login')
      .set('Content-type', 'application/json')
      .send(accountCoca);

    const response = await request(app)
      .get('/room')
      .set('Authorization', `Bearer ${accountResponse.body.accessToken}`)
      .send();

    expect(typeof response.body).toBe('object');
  });
});
