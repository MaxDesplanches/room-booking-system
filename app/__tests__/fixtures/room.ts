const createRooms = (prefix: string) => [1, 2].map((index: number) => {
  const formattedRoomNumber = (`0${index}`).slice(-2);
  return { name: `${prefix}${formattedRoomNumber}` };
});

export const cocaRoom = createRooms('C');
export const pepsiRoom = createRooms('P');
