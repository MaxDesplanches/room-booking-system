const { defaults: tsjPreset } = require('ts-jest/presets');

module.exports = {
  preset: '@shelf/jest-mongodb',
  testEnvironment: 'node',
  verbose: true,
  moduleNameMapper: {
    '@exmpl/(.*)': '<rootDir>/src/$1',
  },
  modulePathIgnorePatterns: ['./__tests__/fixtures'],
  transform: tsjPreset.transform,
};
