# Introduction

Room booking system is a project for employees and business partners of Coke and Pepsi company.

# Business problem

Two companies, COKE and PEPSI, are sharing an office building but as they are
competitors, they don’t trust each other. Tomorrow is COLA day (for one day), that the
two companies are celebrating. They are gathering a number of business partners in
the building. In order to optimize space utilization, they have decided to set-up a joint
booking system where any user can book one of the 20 meeting rooms available, 10
from each company (C01, C02, ..., C10 and P01, P02, ...., P10).
The booking system has the following functionalities:
- Users can see meeting rooms availability
- Users can book meeting rooms by the hour (first come first served)
- Users can cancel their own reservations

# Sequence Diagram

User books a meeting room:

![Sequence diagram](diagrams/sequence.png " Sequence Diagram")

# Algorithms

Instead giving the choice to choose the room to book, the user send a start date and time for the meeting, and the API choose the best room for the user for one hour.  
  
API Behavior:  
  
a. Avoid schedule of a room after the concurrent (if the board was not clean previously)  
b. Avoid schedule in a room beside a concurrent (avoid crossing paths in the corridors and listening to discussions, avoid listening if isolation is bad between room or see board if there is glass ...)  
  
a. solution is not implemented, I choose b., more side effect.  
  
Choosen solution (Coca for instance), best case from 1 to 3 by priority:  
  
1. Start to find an available room, from C01 to C10 room, with solution b.  
2. If no room is found for 1., try to find from P10 to P01 with behavior from 1.  
3. If no room is found for 2., take the one with at least the same company on one side or an empty room
4. If no room is found for 3, take any available room, without condition  
  
Pepsi: Same behavior as Coca, BUT start with room from P10 to P01, then C01 to C10 

# Algorithms optimization (not implemented)  

Divide room by 4 and execute algorithms concurrently in each group  
Not so much rooms to take the time to do it. (can be efficient with a big amount of data)  

# TODO

- use bcrypt to hash password
- check if reservation date is before current date  
- get all rooms return only empty and the one from the same company (refer to JWT)  
- booking reservation start with room from same company (see 1.)  
- finish implementation steps for finder

# Security

- JWT for authentification
- Rate limiter (request)
- express validator for query
- Vault for production password (manual)
  
# Launch (development)  
  
docker-compose up  
  
# Setup (production)  

Change password for database in:  
./ops/prod/Dockerfile.mongo  
./ops/prod/mongo-init.js  
./docker-compose-prod.yml  

create /app/.env.prod from /app/.env.dist and change mongodb id  
  
Ask vault (NordPass) to max.desplanches@gmail.com for current  
   
# Launch (production)  
  
docker-compose -f docker-compose.prod.yml up  
  
# Update package-lock.json and node_modules locally  
  
docker run -it --rm --name npm-install -v "$PWD":/usr/src/app -w /usr/src/app node:16.2.0 npm install
  
# OpenAPI Documentation  
  
available on /api-documentation  
