// Default user creation

db.createUser({
    // TODO: change id
    user: 'user',
    pwd: 'pass',
    roles: [
        {
            role: 'readWrite',
            db: 'booking_service',
        },
    ],
});

// Collections Creation

db = new Mongo().getDB("booking_service");
db.createCollection('rooms', { capped: false });
db.createCollection('accounts', { capped: false });

// Rooms Creation

function createRooms(prefix) {
    for (let index = 1; index < 11; index++) {
        const formattedRoomNumber = ("0" + index).slice(-2);
        db.rooms.insertOne(
            { "name": `${prefix}${formattedRoomNumber}` },
        );
    }
}

createRooms('C');
createRooms('P');

// Accounts Creation

db.accounts.insertMany([
    { "login": "coca_user@coca.com", "password": "pass" },
    { "login": "pepsi_user@pepsi.com", "password": "pass" },
]);
